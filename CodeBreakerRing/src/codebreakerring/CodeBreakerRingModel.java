
package codebreakerring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CodeBreakerRingModel {
    
    private int[] marbles = new int[6]; // record of the code
    private int[] choice = new int[6];  // record of the user's choices
    
    private boolean win;
    private int attempts;
    private int ballPos; // keep track of marble position on display
    private int currentPos; // keep track of recently chosen position
                            // = ball display position - 1
    
    public void newGame() {
        
       attempts = 0;
       ballPos = 0; 
       currentPos = 0; 
       win = false;
       
       // Generate a random code sets of 6 distinct elements out of 8 colors
       Integer[] rangeArray = new Integer[8];
       for(int i=0; i<rangeArray.length; i++) {
           rangeArray[i] = i;
       }
       // create an array list from an array contains 8 integer from 0 to 7
       ArrayList<Integer> range = new ArrayList<>(Arrays.asList(rangeArray));       
       Collections.shuffle(range);   // shuffle to gain a random set code
       // create a new array list which has the first 6 random number 
       ArrayList code = new ArrayList<>(range.subList(0, 6));
       for(int i=0; i<marbles.length; i++) {    // copy to marbles array
           marbles[i] = (int)code.get(i);
       }
       
       // reset the choices
       for(int i=0; i<choice.length; i++){
           choice[i]=8;
       }
       System.out.println(Arrays.toString(marbles));//
    }
    
    public int getAttempts() { 
        return attempts;
    }
    
    public int[] getResult() {
        return marbles;
    }
    
    public int[] getChoice() {
        return choice;
    }
    
    public int[] getStatus() {
        int[] finalstatus = new int[6];
        // create a array of Integer objects to use reverse order function
        Integer[] status = new Integer[6];
        /* Loop in order to create an array of right/not right positions
         with 2 is not in right position and 1 is in right position */
        // find the right chosen colors which are in the code
        for(int i=0; i<6; i++){
            for(int j=0; j<6; j++){
                if(choice[i]==marbles[j]) { 
                    status[i]=1;
                    break;
                }               
                else {
                    status[i]=0;
                }
            }
            // remove the duplicate chosen colors
            for(int k=0; k<6; k++) {
                if(i==k){
                    k++;
                }
                else if(choice[i]==choice[k]) {
                    status[i]=0;
                }
            }
        }
        // find the color in the right position
        for(int i=0; i<6; i++){
            if(marbles[i]==choice[i]) {
                status[i]=2;
            }
        }
        // sort the array in the descending order
        Arrays.sort(status, Collections.reverseOrder());
        // copy the ordered values to the int array to return final status
        for(int i=0; i<finalstatus.length; i++) {
            finalstatus[i] = status[i];
        }
        return finalstatus;
    }
    
    public int getCurrentPosition() {
        if(ballPos==0) {
            currentPos = 5;
        }
        else {
            currentPos = ballPos - 1;
        }
        return currentPos;

    }
    
    public int getBallPosition() {
        return ballPos;
    }
    
    public boolean getWin() {
       return win;
    }
    
    // each time user choose accept, the ball position will shift to the next
    // one and also check if the user have won
    public void accept() { 
        if(ballPos==5) {
            ballPos = 0;            
        }
        else {
            ballPos++;
        }
        attempts++;
        if(Arrays.equals(marbles, choice)){
            win = true;
        }
    }
    
    public void setMarble(int color) {
        choice[ballPos] = color;
    }
            
    public int getScore() {
        int score = 0;
        if(attempts == 6) {
            score = 1000;
        }
        else if(attempts>6 && attempts<=12) {
            score = 750;
        }
        else if(attempts>12 && attempts<=36) {
            score = 500;
        }
        else if(attempts>36 && attempts<=42) {
            score = 250;
        }
        else if(attempts>42 && attempts<=60) {
            score = 100;
        }
        else if(attempts>60 && attempts<=72) {
            score = 50;
        }
        else if(attempts>72) {
            score = 0;
        }
        return score;
    }
    
    public void exit() {
        System.exit(0);
    }
    
}
