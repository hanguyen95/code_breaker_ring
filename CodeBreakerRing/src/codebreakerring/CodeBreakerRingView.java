package codebreakerring;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.BooleanControl;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

public class CodeBreakerRingView extends JFrame {

    private JButton accept = new JButton(new ImageIcon("img/acceptBtn.png"));
    private JLabel attempts = new JLabel("0");
    private JLabel newgame = new JLabel("  Normal mode", 
                new ImageIcon("img/normalBtn.png"), SwingConstants.LEFT);
    private JLabel score = new JLabel("0");
    private JButton giveup = new JButton(new ImageIcon("img/giveupBtn.png"));
    private JLabel quit = new JLabel("QUIT", SwingConstants.LEFT);
    private JLabel ball[] = new JLabel[8]; // array represented 8 colors for choosing
    private JLabel hole[] = new JLabel[6]; // array represented 6 chosen marbles
    private JLabel status[] = new JLabel[6]; // array reperesented 6 status of marbles
    private JButton image[] = new JButton[4];// array represented 4 marble options
    private JLabel win = new JLabel();
    private JLabel time = new JLabel("Guess");
    private JLabel timeattack = new JLabel("  Time attack mode", 
                new ImageIcon("img/timeBtn.png"), SwingConstants.LEFT);
    private JLabel sound = new JLabel(new ImageIcon("img/soundOnBtn.png"));
    private HighLight glass = new HighLight(0);
    private int marbleImage = 1;
    private JLabel background = new JLabel(new ImageIcon("img/bg_" 
                                        + marbleImage + ".jpg"));
    private Font timeFont = new Font("Myriad Pro", Font.PLAIN, 17);
    
    boolean soundOn = true;
    BooleanControl muteControl;
    
    public JButton getAccept() {
        return accept;
    }

    public JLabel getAttempts() {
        return attempts;
    }

    public JLabel getNewgame() {
        return newgame;
    }

    public JLabel getScore() {
        return score;
    }

    public JButton getGiveup() {
        return giveup;
    }

    public JLabel getQuit() {
        return quit;
    }

    public JLabel[] getBall() {
        return ball;
    }

    public JLabel[] getHole() {
        return hole;
    }

    public JLabel[] getStatus() {
        return status;
    }

    public JButton[] getImage() {
        return image;
    }

    public JLabel getWin() {
        return win;
    }

    public JLabel getTime() {
        return time;
    }

    public JLabel getTimeattack() {
        return timeattack;
    }

    public JLabel getSound() {
        return sound;
    }

    public HighLight getGlass() {
        return glass;
    }

    public int getMarbleImage() {
        return marbleImage;
    }

    public boolean isSoundOn() {
        return soundOn;
    }
    
    public CodeBreakerRingView() throws LineUnavailableException, IOException {
        // Get the image set
        setImage();

        // Subpanel for display code set of 6 marbles
        JPanel p1 = new JPanel();
        p1.setLayout(new BorderLayout(0, 25));
        p1.add(hole[0], BorderLayout.NORTH);
        JPanel subp11 = new JPanel();
        subp11.setLayout(new GridLayout(2, 1, 0, 87));
        subp11.add(hole[1]);
        subp11.add(hole[2]);
        p1.add(subp11, BorderLayout.EAST);
        p1.add(hole[3], BorderLayout.SOUTH);
        JPanel subp12 = new JPanel();
        subp12.setLayout(new GridLayout(2, 1, 0, 87));
        subp12.add(hole[5]);
        subp12.add(hole[4]);
        p1.add(subp12, BorderLayout.WEST);
        p1.setBorder(BorderFactory.createEmptyBorder(86, 0, 0, 0));

        // Win message
        Font winfont = new Font("Tahoma", Font.PLAIN, 20);
        win.setFont(winfont);
        win.setPreferredSize(new Dimension(174, 30));
        win.setForeground(Color.WHITE);
        p1.add(win, BorderLayout.CENTER);

        // Subpanel for display status of marbles
        JPanel p2 = new JPanel();
        p2.setLayout(new GridLayout(6, 1, 0, 19));
        for (JLabel marble : status) {
            p2.add(marble);
        }
        p2.setBorder(BorderFactory.createEmptyBorder(90, 63, 0, 0));

        // Panel display colors for choosing
        JPanel p4 = new JPanel();
        p4.setLayout(new GridLayout(8, 1, 0, 17));
        for (JLabel marble : ball) {
            p4.add(marble);
        }
        p4.setBorder(BorderFactory.createEmptyBorder(67, 27, 0, 73));

        // Panel wrap both panels above
        JPanel p12 = new JPanel();
        p12.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        p12.add(p4);
        p12.add(p1);
        p12.add(p2);
        p12.setBorder(BorderFactory.createEmptyBorder(0, 0, 59, 0));

        // Panel display number of attempts and accept button
        JPanel p3 = new JPanel();
        JPanel tryp = new JPanel();
        tryp.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
        tryp.add(time);
        tryp.add(attempts);
        tryp.setBorder(BorderFactory.createEmptyBorder(0, 100, 0, 106));
        tryp.setOpaque(false);
        accept.setPreferredSize(new Dimension(63, 29));
        giveup.setPreferredSize(new Dimension(66, 29));
        p3.setLayout(new BorderLayout());
        p3.add(tryp, BorderLayout.CENTER);
        p3.add(accept, BorderLayout.WEST);
        p3.add(giveup, BorderLayout.EAST);
        p3.setBorder(BorderFactory.createEmptyBorder(0, 33, 0, 0));
        p3.setOpaque(false);

        //acceptBtn
        accept.setOpaque(false);
        accept.setContentAreaFilled(false);
        accept.setBorderPainted(false);
        accept.setBorder(null);

        //giveupBtn
        giveup.setOpaque(false);
        giveup.setContentAreaFilled(false);
        giveup.setBorderPainted(false);
        giveup.setBorder(null);

        // Panel display the game process
        JPanel display = new JPanel();
        display.setLayout(new BorderLayout(0, 0));
        display.add(p12, BorderLayout.NORTH);
        display.add(p3, BorderLayout.CENTER);
        display.setOpaque(false);

        // New Game option
        Border ngpadding = BorderFactory.createEmptyBorder(3, 15, 3, 15);
        Font ngfont = new Font("Myriad Pro", Font.PLAIN, 11);
        newgame.setBorder(BorderFactory.createCompoundBorder(null,
                ngpadding));
        newgame.setFont(ngfont);
        newgame.setForeground(Color.WHITE);

        // Info panel
        JLabel info1 = new JLabel("Code Breaker Answer Marbles");
        JLabel info2 = new JLabel("In code, in right position",
                new ImageIcon("img/redPeg_2.png"), JLabel.LEFT);
        info2.setHorizontalTextPosition(JLabel.RIGHT);
        JLabel info3 = new JLabel("In code, not in right position",
                new ImageIcon("img/whitePeg_2.png"), JLabel.LEFT);
        info3.setHorizontalTextPosition(JLabel.RIGHT);
        info3.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
        JPanel info = new JPanel();
        info.setLayout(new GridLayout(3, 1, 0, 10));
        info.setBorder(BorderFactory.createCompoundBorder(null,
                ngpadding));
        info.add(info1);
        info.add(info2);
        info.add(info3);

        info.setOpaque(false);
        info1.setForeground(Color.WHITE);
        info2.setForeground(Color.WHITE);
        info3.setForeground(Color.WHITE);

        // Score info
        JLabel scorelabel = new JLabel("SCORE");
        JPanel scorep = new JPanel();
        scorep.setLayout(new BorderLayout());
        scorelabel.setFont(new Font("Myriad Pro", Font.PLAIN, 18));
        scorelabel.setForeground(Color.WHITE);
        score.setFont(new Font("Myriad Pro", Font.PLAIN, 18));
        score.setForeground(Color.WHITE);

        scorep.add(scorelabel, BorderLayout.WEST);
        scorep.add(score, BorderLayout.EAST);
        scorep.setBorder(BorderFactory.createCompoundBorder(null,
                ngpadding));
        scorep.setOpaque(false);
        score.setOpaque(false);

        // Quit option
        quit.setBorder(BorderFactory.createCompoundBorder(null,
                ngpadding));
        quit.setFont(new Font("Myriad Pro", Font.PLAIN, 20));
        quit.setForeground(Color.WHITE);

        quit.setOpaque(false);

        // Try attempts format
        Font font1 = timeFont;
        attempts.setFont(font1);
        attempts.setPreferredSize(new Dimension(20, 40));
        attempts.setForeground(Color.WHITE);

        // Timer format
        timeattack.setBorder(BorderFactory.createCompoundBorder(null,
                ngpadding));
        timeattack.setFont(ngfont);
        timeattack.setForeground(Color.WHITE);

        // Countdown clock format
        time.setFont(font1);
        time.setForeground(Color.WHITE);
        time.setPreferredSize(new Dimension(53, 40));

        // Control and info panel
        JPanel control = new JPanel();
        control.setSize(440, 200);
        control.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 3;
        c.gridy = 0;
        c.gridwidth = 4;
        c.insets = new Insets(3, 0, 3, 0);
        control.add(sound, c);
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 4;
        c.insets = new Insets(3, 0, 3, 0);
        control.add(newgame, c);
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 4;
        c.insets = new Insets(3, 0, 3, 0);
        control.add(timeattack, c);
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 4;
        c.insets = new Insets(3, 0, 3, 0);
        control.add(info, c);
        c.gridx = 0;
        c.gridy = 4;
        c.gridwidth = 4;
        c.insets = new Insets(3, 0, 3, 0);
        control.add(scorep, c);
        for (int i = 0; i < image.length; i++) {
            c.gridx = i;
            c.gridy = 6;
            c.gridwidth = 1;
            c.weightx = 0.25;
            c.insets = new Insets(3, -7, 10, -7);
            control.add(image[i], c);
        }
        c.gridx = 0;
        c.gridy = 7;
        c.gridwidth = 4;
        control.add(quit, c);

        control.setOpaque(false);

        //set opaque panels
        p12.setOpaque(false);
        subp11.setOpaque(false);
        subp12.setOpaque(false);
        p1.setOpaque(false);
        p2.setOpaque(false);
        p4.setOpaque(false);

        setContentPane(background);
        // frame format
        setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        add(display);
        add(control);
        getContentPane().setPreferredSize(new Dimension(700, 480));
        pack();
        setLocationRelativeTo(null);
        setTitle("Code Breaker Ring");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);

        // glass panel for displaying highlight
        setGlassPane(glass);
        glass.setOpaque(false);
        glass.setVisible(true);
        
        try {
            // Open an audio input stream.
            File bgMusic = new File("sound/bg.wav");
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(bgMusic);
            // Get a sound clip resource.
            Clip clip = AudioSystem.getClip();
            // Open audio clip and load samples from the audio input stream.
            clip.open(audioIn);
            muteControl = 
                    (BooleanControl) clip.getControl(BooleanControl.Type.MUTE);
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }

    }

    public void setImage() {
        for (int i = 0; i < ball.length; i++) {
            ball[i] = new JLabel(new ImageIcon("img/shadow_ball_" 
                             + marbleImage + "_" + (i + 1) + ".png"));
        }

        // Build status of marbles labels
        for (int i = 0; i < hole.length; i++) {
            hole[i] = new JLabel(new ImageIcon("img/hole_" 
                                        + marbleImage + ".png"));
        }

        // Build display marbles labels
        for (int i = 0; i < status.length; i++) {
            status[i] = new JLabel(new ImageIcon("img/emptyPeg_"
                                            + marbleImage + ".png"));
        }

        // Build image option buttons
        for (int i = 0; i < image.length; i++) {
            int j = i + 1;
            image[i] = new JButton(new ImageIcon("img/hole_" + j + ".png"));
            image[i].setOpaque(false);
            image[i].setContentAreaFilled(false);
            image[i].setBorderPainted(false);
        }
    }

    public void setAttempts(int attempts) {
        this.attempts.setText(Integer.toString(attempts));
    }

    public void setStatus(int[] stat) {
        for (int i = 0; i < stat.length; i++) {
            if (stat[i] == 2) {
                status[i].setIcon(new ImageIcon("img/redPeg_" 
                                                + marbleImage + ".png"));
            } else if (stat[i] == 1) {
                status[i].setIcon(new ImageIcon("img/whitePeg_"
                                                + marbleImage + ".png"));
            } else {
                status[i].setIcon(new ImageIcon("img/emptyPeg_" 
                                                + marbleImage + ".png"));
            }
        }
    }

    public void giveUp(int[] result) {
        for (JLabel statu : status) {
            statu.setIcon(new ImageIcon("img/emptyPeg_"
                                        + marbleImage + ".png"));
        }
        for (int i = 0; i < hole.length; i++) {
            hole[i].setIcon(new ImageIcon("img/piece_" + marbleImage + "_"
                    + (result[i] + 1) + ".png"));
        }
        background.setIcon(new ImageIcon("img/lose_" + marbleImage + ".jpg"));
        setContentPane(background);
    }

    public void win(int score) {
        this.score.setText(Integer.toString(score));
        background.setIcon(new ImageIcon("img/win_" + marbleImage + ".gif"));
        setContentPane(background);
        win.setText("");
    }

    public void timerLost() {
        background.setIcon(new ImageIcon("img/lose_" + marbleImage + ".jpg"));
        setContentPane(background);
        win.setText("");
    }

    public void timerSet(int t) {
        Font font = timeFont;
        time.setFont(font);
        time.setForeground(Color.WHITE);
        // format the time display
        if (t / 60 < 10 && t % 60 < 10) {
            time.setText("0" + t / 60 + ":" + "0" + t % 60);
        } else if (t / 60 <= 10 && t % 60 >= 10) {
            time.setText("0" + t / 60 + ":" + t % 60);
        } else {
            time.setText(t / 60 + ":" + t % 60);
        }
    }

    public void normalModeText() {
        time.setText("Guess");
    }

    public void setImageColor(int a) {
        marbleImage = a;
        background.setIcon(new ImageIcon("img/bg_" + marbleImage + ".jpg"));
        setContentPane(background);
        for (int i = 0; i < ball.length; i++) {
            ball[i].setIcon(new ImageIcon("img/shadow_ball_" + marbleImage + "_"
                    + (i + 1) + ".png"));
        }
    }

    public void setHole(int position, int color) {

        if (color == 8) {
            hole[position].setIcon(new ImageIcon("img/hole_" 
                                            + marbleImage + ".png"));
        } else {
            hole[position].setIcon(new ImageIcon("img/piece_" + marbleImage
                    + "_" + (color + 1) + ".png"));
        }
    }

    public void hoverBall(int i) {
        ball[i].setIcon(new ImageIcon("img/shadow_ball_" 
                        + marbleImage + "_" + (i + 1) + "_hover.gif"));

    }

    public void exitBall(int i) {
        ball[i].setIcon(new ImageIcon("img/shadow_ball_"
                            + marbleImage + "_" + (i + 1) + ".png"));

    }

    public void newGame() {
        for (JLabel marble : hole) {
            marble.setIcon(new ImageIcon("img/hole_" + marbleImage + ".png"));
        }
        for (JLabel marble : status) {
            marble.setIcon(new ImageIcon("img/emptyPeg_"+marbleImage + ".png"));
        }
        background.setIcon(new ImageIcon("img/bg_" + marbleImage + ".jpg"));
        setContentPane(background);
        win.setText("");
    }
    
    public void changeSound() {
        soundOn = !soundOn;
        if (soundOn){
            sound.setIcon(new ImageIcon("img/soundOnBtn.png"));
        } else {
            sound.setIcon(new ImageIcon("img/soundOffBtn.png"));
        }
        muteControl.setValue(!soundOn);
    }

    public void acceptListener(ActionListener listenAccept) {
        accept.addActionListener(listenAccept);
    }

    public void acceptMouseListener(MouseListener listenAccept) {
        accept.addMouseListener(listenAccept);
    }

    public void newGameListener(MouseListener listenNewGame) {
        newgame.addMouseListener(listenNewGame);
    }

    public void timeListener(MouseListener listenTime) {
        timeattack.addMouseListener(listenTime);
    }

    public void chooseMarbleListener(MouseListener listenChoose) {
        for (JLabel marble : ball) {
            marble.addMouseListener(listenChoose);
        }
    }

    public void quitListener(MouseListener listenQuit) {
        quit.addMouseListener(listenQuit);
    }

    public void giveUpListener(MouseListener listenGiveUp) {
        giveup.addMouseListener(listenGiveUp);
    }

    public void imageOptionListener(ActionListener listenImage) {
        for (JButton image1 : image) {
            image1.addActionListener(listenImage);
        }
    }

    public void soundListener(MouseListener listenSound) {
        sound.addMouseListener(listenSound);
    }

    public void imageOptionMouseListener(MouseListener listenImage) {
        for (JButton image1 : image) {
            image1.addMouseListener(listenImage);
        }
    }

    // new class to draw highlight circle corresponded to the chosen position
    public class HighLight extends JPanel {

        private int pos;

        public HighLight(int pos) {
            super();
            this.pos = pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(Color.WHITE);
            Graphics2D circle = (Graphics2D) g;
            circle.setStroke(new BasicStroke(2));
            // turn on aliasing to have a nice drawing
            circle.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            //Draw circle marking the current position
            switch (pos) {
                case 0:
                    circle.draw(new Ellipse2D.Double(220, 83, 45, 45));
                    break;
                case 1:
                    circle.draw(new Ellipse2D.Double(323, 141, 45, 45));
                    break;
                case 2:
                    circle.draw(new Ellipse2D.Double(323, 260, 45, 45));
                    break;
                case 3:
                    circle.draw(new Ellipse2D.Double(220, 318, 45, 45));
                    break;
                case 4:
                    circle.draw(new Ellipse2D.Double(115, 260, 45, 45));
                    break;
                case 5:
                    circle.draw(new Ellipse2D.Double(115, 140, 45, 45));
                    break;
                default:
                    break;
            }
        }
    }
}
