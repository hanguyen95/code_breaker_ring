
package codebreakerring;

import java.io.IOException;
import javax.sound.sampled.LineUnavailableException;

public class CodeBreakerRingMVC {
    
    public static void main(String[] args) throws LineUnavailableException,
                                                                IOException {
        CodeBreakerRingView view = new CodeBreakerRingView();
        CodeBreakerRingModel model = new CodeBreakerRingModel();
        CodeBreakerRingController controller = 
                        new CodeBreakerRingController(view, model);
    }
    
}
