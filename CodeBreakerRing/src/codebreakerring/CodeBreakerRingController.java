package codebreakerring;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;

public class CodeBreakerRingController implements ActionListener, 
                                                        MouseListener {

    private CodeBreakerRingView view;
    private CodeBreakerRingModel model;
    private boolean enableListener; // for disable choosing when end game
    private int interval;  // countdown time 
    private Timer timer;
    private boolean timerIsRunning;

    public CodeBreakerRingController(CodeBreakerRingView view,
            CodeBreakerRingModel model) {
        this.view = view;
        this.model = model;
        view.getAccept().setEnabled(true);
        enableListener = true;
        model.newGame();

        view.acceptListener(this);
        view.acceptMouseListener(this);
        view.newGameListener(this);
        view.chooseMarbleListener(this);
        view.quitListener(this);
        view.giveUpListener(this);
        view.timeListener(this);
        view.imageOptionListener(this);
        view.imageOptionMouseListener(this);
        view.soundListener(this);

    }

    public void setHighLight(int pos) {
        view.getGlass().setPos(pos);
        view.getGlass().repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (enableListener) {
            for (int i = 0; i < view.getImage().length; i++) {
                if (e.getSource() == view.getImage()[i]) {
                    view.setImageColor(i + 1);
                    view.setStatus(model.getStatus());
                    for (int j = 0; j < 6; j++) {
                        view.setHole(j, model.getChoice()[j]);
                    }
                }
            }
        }
        if (e.getSource() == view.getAccept()) {
            model.accept();          
            view.setAttempts(model.getAttempts());
            view.setStatus(model.getStatus());
            setHighLight(model.getBallPosition());
            if (model.getWin()) {
                if(timerIsRunning) {
                    timer.cancel();
                }
                view.win(model.getScore());
                view.getAccept().setEnabled(false);
                enableListener = false;
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (enableListener == true) {
            for (int i = 0; i < view.getBall().length; i++) {
                if (e.getSource() == view.getBall()[i]) {
                    model.setMarble(i);
                    view.setHole(model.getBallPosition(), i);
                }
            }
        }
        if (e.getSource() == view.getNewgame()) {
            view.getAccept().setEnabled(true);
            enableListener = true;
            view.normalModeText();
            if (timerIsRunning == true) {
                timer.cancel();
            }
            model.newGame();
            view.newGame();
            view.setAttempts(model.getAttempts());
            setHighLight(model.getBallPosition());
        } else if (e.getSource() == view.getQuit()) {
            model.exit();
        } else if (e.getSource() == view.getGiveup() && enableListener) {
            view.giveUp(model.getResult());
            view.getAccept().setEnabled(false);
            enableListener = false;
            if (timerIsRunning == true) {
                timer.cancel();
            }
        } else if (e.getSource() == view.getTimeattack()) {
            view.getAccept().setEnabled(true);
            enableListener = true;
            timer = new Timer();
            interval = 90;  // set countdouwn time is 90 seconds
            view.newGame();
            model.newGame();
            view.setAttempts(model.getAttempts());
            setHighLight(model.getBallPosition());
            view.timerSet(interval);
            // create a countdown clock 
            timer.scheduleAtFixedRate(new TimerTask() {

                @Override
                public void run() {
                    timerIsRunning = true;
                    if (interval == 0) {
                        timer.cancel();
                        view.timerLost();
                        view.giveUp(model.getResult());
                        view.getAccept().setEnabled(false);
                        enableListener = false;
                        timerIsRunning = false;
                    } else {
                        --interval;
                        view.timerSet(interval);
                    }
                }
            }, 1000, 1000);
        } else if (e.getSource() == view.getSound()) {
            view.changeSound();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (enableListener == true) {
            for (int i = 0; i < view.getBall().length; i++) {
                if (e.getSource() == view.getBall()[i]) {
                    view.hoverBall(i);
                }
            }
        }

        if (e.getSource() == view.getAccept() && enableListener) {
            view.getAccept().setIcon(new ImageIcon("img/acceptBtn_hover.png"));
        } else if (e.getSource() == view.getGiveup() && enableListener) {
            view.getGiveup().setIcon(new ImageIcon("img/giveupBtn_hover.png"));
        } else if (e.getSource() == view.getNewgame()) {
            view.getNewgame().setIcon(new ImageIcon("img/normalBtn_hover.png"));
        } else if (e.getSource() == view.getTimeattack()) {
            view.getTimeattack().setIcon
                                    (new ImageIcon("img/timeBtn_hover.png"));
        }

        if (view.soundOn && enableListener) {
            try {
                // Open an audio input stream.
                File bgMusic = new File("sound/mouseOver.wav");
                AudioInputStream audioIn = 
                            AudioSystem.getAudioInputStream(bgMusic);
                // Get a sound clip resource.
                Clip clip = AudioSystem.getClip();
                // Open audio clip and load samples from the audio input stream.
                clip.open(audioIn);
                clip.start();
            } catch (UnsupportedAudioFileException | IOException 
                                        | LineUnavailableException f) {
                f.printStackTrace();
            }
        }
        view.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (enableListener == true) {
            for (int i = 0; i < view.getBall().length; i++) {
                if (e.getSource() == view.getBall()[i]) {
                    view.exitBall(i);
                }
            }
        }
        if (e.getSource() == view.getAccept() && enableListener) {
            view.getAccept().setIcon(new ImageIcon("img/acceptBtn.png"));
        } else if (e.getSource() == view.getGiveup() && enableListener) {
            view.getGiveup().setIcon(new ImageIcon("img/giveupBtn.png"));
        } else if (e.getSource() == view.getNewgame()) {
            view.getNewgame().setIcon(new ImageIcon("img/normalBtn.png"));
        } else if (e.getSource() == view.getTimeattack()) {
            view.getTimeattack().setIcon(new ImageIcon("img/timeBtn.png"));
        }
        view.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
}
