This is a implementation of the guessing game called Code Breaker Ring (refer to this http://codebreaker.creativitygames.net/codebreaker_ring.php or you can search other sources for more details on how the game works). The goal is to guess a secret code of Ring length six, with each element of the code chosen from a list of 8 different colors. For example, the secret code could be (red, green, blue, orange, red, purple), and the goal is to guess it in as few tries as possible.
This game is implemented in a GUI following the MVC model.

###Language:
Java, Java Swing

###Contributors:
* Ha Nguyen
* Huy Tran Tan